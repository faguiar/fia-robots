#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import pygame

from mine import Mine
from board import Board
from robot import Robot
from base import Base

def main():
	pygame.init()
	size = width, height = 640, 480	
	screen = pygame.display.set_mode(size)
		
	b = Board(screen, sys.argv[1])
	
	while(1):
		for event in pygame.event.get():
			if event.type == pygame.QUIT: sys.exit()		
		pygame.display.flip()
	
	return 0

if __name__ == '__main__':
	main()

