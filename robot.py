#!/usr/bin/env python
# -*- coding: utf-8 -*-

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame, sys
import random
from mine import Mine
from wall import Wall
from clue import Clue
STEP = 5

class Robot():
	def __init__(self, board, coords):
		self.screen = board.screen
		self.board = board
		
		#Scoreboard
		self.score = 0
		self.font=pygame.font.Font(None,18)
		self.scoreboard = self.font.render("Total gathered ores:", 1, (255,255,255))
		self.scoreposx = 5
		self.board.screen.blit(self.scoreboard, (self.scoreposx, 5))
		
		
		self.x = coords[0]
		self.y = coords[1]
		self.carrying_ore = False
		
		#First random location
		self.destiny = (random.randint(10,630), random.randint(10, 470))
		
		self.rect = pygame.Rect((self.x, self.y),(12,12))
		self.destinyrect = pygame.Rect((self.destiny[0],self.destiny[1]),(40,40))

	def draw(self):
		pygame.draw.rect(self.screen, (0,255,0), (self.x, self.y, 6, 6), 0)
		pygame.display.flip()
		pygame.time.delay(10)

	def erase(self, color=None):
		if(color):
			pygame.draw.rect(self.screen, color, (self.x, self.y, 6,6), 0)
		else:
			if(self.is_insideBase()):
				pygame.draw.rect(self.screen, (0,0,255), (self.x, self.y, 6,6), 0)
			else:
				pygame.draw.rect(self.screen, (0,0,0), (self.x, self.y, 6,6), 0)
	
	def erase_destiny(self):
		self.destinyrect = pygame.Rect((0,0),(5,5))
		#print "apagay"
		return True
	
	#Deprecated, migrar para o metodo abaixo assim que possivel
	def choose_coordinates(self):
		while(not(self.check_collision())):
			#print "-- Choosing a New Destiny to Robot # ... \n"
			self.destiny = (random.randint(10,630), random.randint(10, 470))
			self.destinyrect = pygame.Rect((self.destiny[0],self.destiny[1]),(12,12))
		return True
	
	#new method, start to use it
	def roll_coordinates(self):
		#print "-- Robot has arrived, generate new route ... \n"
		self.destiny = (random.randint(10,630), random.randint(10,470))
		self.destinyrect = pygame.Rect((self.destiny[0], self.destiny[1]), (40,40))
		return True
		
	def move_robot(self, x = None, y = None):
		self.erase()

		if x is None:
			x = self.x
		if y is None:
			y = self.y

		if (self.has_arrived()):
			self.roll_coordinates()

		if(x > self.destiny[0]):
			x = x - STEP
		elif(x < self.destiny[0]):
			x = x + STEP

		if(y > self.destiny[1]):
			y = y - STEP
		elif(y < self.destiny[1]):
			y = y + STEP

		if(self.check_collision(x, y)):
			self.move_robot(x, y)
			return

		self.x = x
		self.y = y
		self.rect.x = self.x
		self.rect.y = self.y
		
		#If inside base, deposit carrying ore
		if((self.is_insideBase()) and (self.carrying_ore)):
			self.deposit_ore() 

		self.draw()

		return True

	def handle_collision(self):
		if random.randint(1,2) % 2 == 0:
			self.x += random.randint(0,1)*(STEP*random.randint(-2,2))
			self.y += random.randint(0,1)*(STEP*random.randint(-2,2))
		else:
			self.x -= random.randint(0,1)*(STEP*random.randint(-2,2))
			self.y -= random.randint(0,1)*(STEP*random.randint(-2,2))
		self.check_collision()

	def has_arrived(self):
		if(self.destinyrect.collidepoint(self.x,self.y)):
			#print "chegay"
			self.erase_destiny()
			return True
		elif(self.destinyrect.colliderect(self)):
			self.erase_destiny()
			return True
			
		else:
			return False

	def is_insideBase(self):
		if(self.rect.colliderect(self.board.base.rect)):
			return True
		else:
			return False

	def check_collision(self, x = None, y = None):
		for position in self.board.board_map.keys():
			obj = self.board.board_map[position]
			if self.rect.colliderect(obj.rect):
				if(isinstance(obj, Mine)):
					self.gather_ore(obj)
				if(isinstance(obj, Wall)):
					self.roll_coordinates()

		if x is None:
			x = self.x
		if y is None:
			y = self.y

		if self.board.grid[x][y] == 1:
			return True

		return False
	
	def gather_ore(self, obj):
		if(obj.ores > 0):
			if(not(self.carrying_ore)):
				obj.collect()
				self.carrying_ore = True
				return True
			else:
				return False
		else:
			return False

	def deposit_ore(self):
		if(self.carrying_ore):
			self.carrying_ore = False
			self.score += 1
			self.update_scoreboard()
			print "Depositou na base"
			return True
		return False
		
	def update_scoreboard(self):
		self.font=pygame.font.Font(None,18)
		self.scoreposx = self.scoreposx+10
		self.scoreboard = self.font.render('+', 1, (255,255,255))
		self.scoreposx += 5
		self.board.screen.blit(self.scoreboard, (self.scoreposx, 15))
		return True

	def escape(self, obj):
		self.erase((0,0,0))
		self.destiny = (random.randint(10,630), random.randint(10, 470))
		self.x = 300
		self.y = 40
		return True
		
		
class Comportamento1(Robot):
		def __init__(self,board, coords):
				Robot.__init__(self, board, coords)		

class Comportamento2(Robot):
		def __init__(self,board, coords):
				Robot.__init__(self,board, coords)

		def gather_ore(self, obj):
			if(obj.ores > 0):
				if(not(self.carrying_ore)):
					obj.collect()
					self.carrying_ore = True
					self.destiny = (self.board.base.x, self.board.base.y)
					return True
				else:
					return False
			else:
				return False

		def deposit_ore(self):
			if(self.carrying_ore):
				self.carrying_ore = False
				self.score += 1
				self.update_scoreboard()
				self.roll_coordinates()
				print "Depositou na base"
				return True
			return False
	
		
		def move_robot(self,x=None,y=None):
			self.erase()

			if x is None:
				x = self.x
			if y is None:
				y = self.y

			if (self.has_arrived()):
				self.roll_coordinates()

			if(x > self.destiny[0]):
				x = x - STEP
			elif(x < self.destiny[0]):
				x = x + STEP

			if(y > self.destiny[1]):
				y = y - STEP
			elif(y < self.destiny[1]):
				y = y + STEP

			if(self.check_collision(x, y)):
				self.move_robot(x, y)
				return

			self.x = x
			self.y = y
			self.rect.x = self.x
			self.rect.y = self.y
			
			#If inside base, deposit carrying ore
			if((self.is_insideBase()) and (self.carrying_ore)):
				self.deposit_ore() 

			self.draw()

			return True

class Comportamento3(Comportamento2):
	def __init__(self,board, coords):
		Comportamento2.__init__(self,board, coords)

	def move_robot(self, x = None, y = None):
		self.erase()

		if x is None:
			x = self.x
		if y is None:
			y = self.y

		if (self.has_arrived()):
			self.roll_coordinates()

		# se estiver voltando pra base
		if (self.carrying_ore):
			# deixa rastro
			self.board.grid[self.x][self.y] = 2
			pygame.draw.rect(self.screen, (255,255,0), (self.x, self.y, 8, 8), 0)
			pygame.display.flip()
		# se esbarrar com um rastro
		elif (self.board.grid[x][y] == 2):
			self.x = x
			self.y = y

		# calcula proximo passo
		if(x > self.destiny[0]):
			x = x - STEP
		elif(x < self.destiny[0]):
			x = x + STEP

		if(y > self.destiny[1]):
			y = y - STEP
		elif(y < self.destiny[1]):
			y = y + STEP

		if(self.check_collision(x, y)):
			self.move_robot(x, y)
			return

		self.x = x
		self.y = y
		self.rect.x = self.x
		self.rect.y = self.y

		#If inside base, deposit carrying ore
		if((self.is_insideBase()) and (self.carrying_ore)):
			self.deposit_ore() 

		self.draw()

		return True

class Comportamento4(Comportamento3):
		def __init__(self,board, coords):
				Comportamento3.__init__(self,board, coords)
				
		def move_robot(self, x = None, y = None):
			self.erase()

			if x is None:
				x = self.x
			if y is None:
				y = self.y

			if (self.has_arrived()):
				self.roll_coordinates()

			# se estiver voltando pra base
			if (self.carrying_ore):
				# deixa rastro
				self.board.grid[self.x][self.y] = 2
				pygame.draw.rect(self.screen, (255,255,0), (self.x, self.y, 8, 8), 0)
				pygame.display.flip()
			# se esbarrar com um rastro
			elif (self.board.grid[x][y] == 2):
				self.x = x
				self.y = y
				self.board.grid[x][y] = 0
				pygame.draw.rect(self.screen, (0,0,0), (self.x, self.y, 15, 15), 0)
				pygame.display.flip()

			# calcula proximo passo
			if(x > self.destiny[0]):
				x = x - STEP
			elif(x < self.destiny[0]):
				x = x + STEP

			if(y > self.destiny[1]):
				y = y - STEP
			elif(y < self.destiny[1]):
				y = y + STEP

			if(self.check_collision(x, y)):
				self.move_robot(x, y)
				return

			self.x = x
			self.y = y
			self.rect.x = self.x
			self.rect.y = self.y

			#If inside base, deposit carrying ore
			if((self.is_insideBase()) and (self.carrying_ore)):
				self.deposit_ore() 

			self.draw()

			return True
