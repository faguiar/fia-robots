import pygame

class Base():

    def __init__(self, screen, coords):
        self.screen = screen
        self.coords = coords
        self.x = coords[0]
        self.y = coords[1]

        self.width = 80
        self.height = 40

    def draw(self):
        self.rect = pygame.draw.rect(self.screen, (0,0,255), (self.coords[0], self.coords[1], 80, 40), 0)
