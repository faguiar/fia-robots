#!/usr/bin/env python
# -*- coding: utf-8 -*-

from mine import Mine
from base import Base
from robot import *
from wall import Wall
import random
import pygame

class T:
	def __init__(self, x, y):
		self.x = x
		self.y = y

class Board():
	
	def __init__(self, screen, num_bots):
		self.screen = screen        
		self.board_map = {}
		self.num_bots = num_bots

		self.grid = [[0 for x in xrange(800)] for x in xrange(800)]

		self.base = Base(screen, (300,20))
		self.add(self.base)

		self.generate_walls(10)
		self.spawn_mines(15)
		self.spawn_robots(5)

	def add(self, obj):
		self.board_map[T(obj.x, obj.y)] = obj

		if isinstance(obj, Wall) or isinstance(obj, Mine):
			self.grid[obj.x][obj.y] = 1
			for x in xrange(obj.x-10, obj.x + obj.width+10):
				for y in xrange(obj.y-10, obj.y + obj.height+10):
					self.grid[x][y] = 1

		obj.draw()
	
	def generate_position(self):
		return (random.randint(20,600), random.randint(20,460))
	
	def generate_walls(self, num):
		for i in range(0, num):
			position = self.generate_position()
			w = Wall(self.screen, position)
			if not self.check_collision(w):
				self.add(w)
	
	def spawn_mines(self, num):
		for i in range(0,num):
			position = self.generate_position()
			m = Mine(self.screen, position)
			
			if not self.check_collision(m):
				self.add(m)
			
	def check_collision(self, obj):
		# Verificar colisoes
		for position in self.board_map.keys():
			other = self.board_map[position]
			if(not(isinstance(obj, Base))):
				if(obj.rect.colliderect(other.rect)):
					return True
		return False

	def spawn_robots(self, num):
		comportamento = self.num_bots
		if comportamento == '1':		
			for i in range(0,num):
				position = (random.randint(self.base.coords[0],self.base.coords[0]+80), random.randint(self.base.coords[1],self.base.coords[1]+40))
				self.add(Comportamento1(self,position))

		if comportamento == '2':		
			for i in range(0,num):
				position = (random.randint(self.base.coords[0],self.base.coords[0]+80), random.randint(self.base.coords[1],self.base.coords[1]+40))
				self.add(Comportamento2(self,position))
		
		if comportamento == '3':		
			for i in range(0,num):
				position = (random.randint(self.base.coords[0],self.base.coords[0]+80), random.randint(self.base.coords[1],self.base.coords[1]+40))
				self.add(Comportamento3(self,position))

		if comportamento == '4':		
			for i in range(0,num):
				position = (random.randint(self.base.coords[0],self.base.coords[0]+80), random.randint(self.base.coords[1],self.base.coords[1]+40))
				self.add(Comportamento4(self,position))

			
		while True:
			for robot in self.board_map.keys():
				if isinstance(self.board_map[robot], Robot):
					self.board_map[robot].move_robot()
