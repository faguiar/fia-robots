#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame, sys

WIDTH = HEIGHT = 20

class Clue():
    def __init__(self, screen, coords):
        self.x = coords[0]
        self.y = coords[1]
        self.screen = screen
        self.coords = coords
        self.rect = pygame.Rect(self.coords[0]-20, self.coords[1]-20, WIDTH*3, HEIGHT*3)

    def draw(self):
        pygame.draw.rect(self.screen, (15,15,255), (self.coords[0], self.coords[1], WIDTH, HEIGHT), 1)


