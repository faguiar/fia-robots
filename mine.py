#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pygame, sys

RED = (255, 0, 0)
BLACK = (0, 0, 0)
WIDTH = 6
HEIGHT = 6
FILL = 1

class Mine():
	def __init__(self, screen, coords):
		self.x = coords[0]
		self.y = coords[1]
		self.screen = screen
		self.coords = coords
		
		self.dead = False
		
		self.rect = pygame.Rect((self.coords[0]-20, self.coords[1]-20, WIDTH + 40 , HEIGHT + 40))

		self.ores = 5

		self.width = 6
		self.height = 6

	def draw(self):
		pygame.draw.rect(self.screen, RED, (self.coords[0], self.coords[1], WIDTH, HEIGHT), FILL)

	def collect(self):
		
		if(self.dead):
			return False
		else:
			self.ores -= 1
			print "Ore gathered from mine"
			print "\n This mine have",
			print self.ores,
			print " ores left...\n"
		if self.ores <= 0:
			self.destroy()

	def destroy(self):
		self.dead = True
		pygame.draw.rect(self.screen, BLACK, (self.coords[0], self.coords[1], WIDTH, HEIGHT), FILL)
